module Main exposing (main)

import Element as Element exposing (Element, Color, el, text, row, column, fill, width, height, rgb255, spacing, centerX, centerY, padding)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Browser
import List
import Set
import Dict exposing (Dict)
import Http
import Json.Decode as D
import Random
import Random.List exposing (shuffle)
import CasusTable exposing (casusTable)
import Html exposing (Html)

-- MAIN

main = Browser.document
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }


-- MODEL

type alias Model =
    { cards : Dict String Card
    , challenges : List Challenge
    , error : Maybe Http.Error
    , attempt : String
    , color : Color
    , prev_result : String
    , prev_german : String
    }

type alias Card =
    { genus : Genus
    , dekl : Deklination
    , genitiv : String
    , genitiv_german : String
    , genitiv_plural : Maybe String
    }

type alias Challenge =
    { genus : Genus
    , kasus : String
    , numerus : String
    , latin : String
    , german : String
    }

fromChallenge : Challenge -> String
fromChallenge challenge
    = challenge.kasus ++ " " ++ challenge.numerus ++ " " ++ fromGenus challenge.genus


generateChallenges : List String -> List String -> List Card -> List Challenge
generateChallenges kasi numeri cards
    = List.foldl (\x acc -> case x of
            Just y -> y::acc
            Nothing -> acc) [] <| List.map3 generateChallenge kasi numeri cards

generateChallenge : String -> String -> Card -> Maybe Challenge
generateChallenge kasus numerus card
    = let stem = case card.dekl of
                    O_DEKL -> String.dropRight 1 card.genitiv
                    _ -> String.dropRight 2 card.genitiv
          key = kasus ++ " " ++ numerus ++ " " ++ fromDekl card.dekl ++ (if card.dekl == O_DEKL then "(" ++ fromGenus card.genus ++ ".)" else "")

          special_endings = case (kasus, numerus, card.genus) of
            ("Nom.", "Pl.", Neutrum) -> Just "-a"
            ("Akk.", "Pl.", Neutrum) -> Just "-a"
            ("Gen.", "Pl.", _) -> card.genitiv_plural
            _ -> Nothing
          ending = case special_endings of
            Just x -> Just x
            Nothing -> Dict.get key casusTable
      in case ending of
            Just e ->
                if kasus == "Nom." && numerus == "Sg."
                    then Nothing
                    else Just
                        { latin = stem ++ (String.dropLeft 1 e)
                        , german = card.genitiv_german
                        , genus = card.genus
                        , kasus = kasus
                        , numerus = numerus
                        }
            Nothing -> Nothing

-- GENUS
type Genus
    = Feminine
    | Maskulin
    | Neutrum
    | NotFound

toGenus : String -> Genus
toGenus str =
    case str of
        "f" -> Feminine
        "m" -> Maskulin
        "n" -> Neutrum
        _ -> NotFound

fromGenus : Genus -> String
fromGenus g =
    case g of
        Feminine -> "f"
        Maskulin -> "m"
        Neutrum -> "n"
        NotFound -> "Not found"

--  DEKLINATION
type Deklination
    = O_DEKL
    | A_DEKL
    | KONS_DEKL

toDekl : String -> Deklination
toDekl str =
    case str of
        "o" -> O_DEKL
        "a" -> A_DEKL
        "kons" -> KONS_DEKL
        _ -> KONS_DEKL

fromDekl : Deklination -> String
fromDekl dekl =
    case dekl of
        O_DEKL -> "o-Dekl."
        A_DEKL -> "a-Dekl."
        KONS_DEKL -> "kons-Dekl."

init : () -> (Model, Cmd Msg)
init _ =
    ({ cards = Dict.empty
     , challenges = []
     , error = Nothing
     , attempt = ""
     , color = blue
     , prev_result = ""
     , prev_german = ""
     }
    , getNouns
    )

-- UPDATE
type Msg
    = GotNouns (Result Http.Error (Dict String Card))
    | TryKNG String
    | Start
    | ShuffledCards (List String, List String, List Card)
    | Skip

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        GotNouns result ->
            case result of
                Ok cards -> ({ model | cards = cards}, Cmd.none)
                Err err -> ({ model | error = Just err }, Cmd.none)
        TryKNG attempt ->
            case model.challenges of
                (challenge::challenges) ->
                    let (kasus, numerus, genus) = findKNG <| String.toLower attempt
                    in if String.startsWith kasus (String.toLower challenge.kasus)
                          && String.startsWith numerus (String.toLower challenge.numerus) && genus == challenge.genus
                        then ({ model | color = green
                                    , attempt = ""
                                    , challenges = challenges
                                    , prev_result = fromChallenge challenge
                                    , prev_german = challenge.german
                                    }, Cmd.none)
                        else ({ model | color = if String.length attempt < 8 then white else red
                                    , attempt = attempt}, Cmd.none)
                _ -> (model, Cmd.none)
        Start ->
            let cardShuffling = shuffle <| Dict.values model.cards
                kasi = Random.list (Dict.size model.cards) <| Random.uniform "Nom." ["Akk.", "Abl.", "Gen.", "Dat."]
                numeri = Random.list (Dict.size model.cards) <| Random.uniform "Sg." ["Pl."]
            in (model, Random.generate ShuffledCards <| Random.map3 (\a b c -> (a,b,c)) kasi numeri cardShuffling)

        Skip ->
            case  model.challenges of
                (challenge::challenges) ->
                    ({ model | challenges = challenges
                            , prev_result = fromChallenge challenge
                            , prev_german = challenge.german
                    }, Cmd.none)
                _ -> (model, Cmd.none)

        ShuffledCards (kasi, numeri, cards) ->
            ({ model | challenges = generateChallenges kasi numeri cards}, Cmd.none)

findKNG : String -> (String, String, Genus)
findKNG attempt
    = let
        numerus = if String.contains "sg" attempt
            then "sg"
            else if String.contains "pl" attempt
                then "pl"
                else "not-found"
        genus = if String.contains "f" attempt
            then Feminine
            else if String.contains "m" attempt
                then Maskulin
                else if String.contains "n" attempt
                    then Neutrum
                    else NotFound
        findKasus acc x = if String.contains x attempt
            then x
            else acc
        kasus = List.foldl findKasus "not-found" ["nom", "akk", "abl", "gen", "dat"]
      in (kasus, numerus, genus)

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model = Sub.none

-- VIEW

blue = Element.rgb255 100 100 200
white = Element.rgb255 255 255 255
green = Element.rgb255 100 200 100
red = Element.rgb255 200 100 100

view : Model -> Browser.Document Msg
view model =
    Browser.Document "Latein - Deklinationsübungen" << List.singleton
      <| Element.layout [centerX, centerY, height fill, width fill] <|
                column [ width fill, height fill, centerX, spacing 30]
                    [ el [Font.size 15, centerX] <| text "Latein Deklinationsübungen"
                    , case List.head model.challenges of
                        Nothing -> Input.button [Background.color blue, centerX, Border.rounded 10, Border.width 10, Border.color blue]
                                    { onPress = Just Start
                                    , label = text "Start"
                                    }
                        Just challenge -> challengeForm model.attempt model.color challenge
                    , case model.error of
                        Just error ->
                            el [Font.color red, centerX] << text <| Debug.toString error
                        Nothing ->
                            el [] <| text ""
                    , el [Font.size 15, centerX] <| text model.prev_result
                    , el [Font.size 15, centerX] <| text model.prev_german
                    ]


challengeForm : String -> Color -> Challenge -> Element Msg
challengeForm attempt color challenge =
    row [spacing 40, centerX]
        [ el [Font.size 20, centerX] <| text challenge.latin
        , Input.text [Background.color color, centerX]
            { onChange = TryKNG
            , text = attempt
            , placeholder = Just <| Input.placeholder [] (text "Akk. Sg. m")
            , label = Input.labelHidden "Eingabe für Kasus-Numerus-Genus"
            }
        , Input.button [Background.color red, centerX, Border.rounded 10, Border.width 10, Border.color red]
            { onPress = Just Skip
            , label = text "Skip"
            }
        ]
-- HTTP
getNouns : Cmd Msg
getNouns =
    Http.get
        { url = "./nouns.json"
        , expect = Http.expectJson GotNouns (D.dict cardDecoder)
        }

cardDecoder : D.Decoder Card
cardDecoder
    = D.map5 Card
        (D.field "genus" <| D.map toGenus D.string)
        (D.field "dekl" <| D.map toDekl D.string)
        (D.field "genitiv" D.string)
        (D.field "genitiv_german" D.string)
        (D.maybe <| D.field "genitiv_plural" D.string)
